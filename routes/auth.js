const express = require('express');
const router = express.Router();
const passport = require('passport');
const {check, validationResult} = require('express-validator');

router.route('/login')
    .get((req, res) => {
        res.render('login', {title: 'Login your account', login: true});
    })
    .post(
        passport.authenticate('local', {
            successRedirect: '/',
            failureRedirect: '/login'
        }),
        (req, res) => {
            // If this function gets called, authentication was successful.
            // `req.user` contains the authenticated user.
            res.redirect('/');
        }
    );

router.route('/register')
    .get((req, res) => {
        res.render('register', {title: 'Register a new account', register: true});
    })
    .post([
        check('name').not().isEmpty().withMessage('Empty name'),
        check('email').isEmail().withMessage('Invalid email'),
        check('confirmPassword', 'Confirm password field must have the same value as the password field')
            .exists()
            .custom((value, {req}) => value === req.body.password),
        check('password').not().isEmpty().withMessage('Empty password'),
        check('password').isLength({min: 7}).withMessage('The password must be longer than 6 characters')
    ], (req, res) => {
        const errors = validationResult(req);

        if (errors.array().length) {
            res.render('register', {
                name: req.body.name,
                email: req.body.email,
                errorMessages: errors.array()
            });
        } else {
            const user = new User();
            user.name = req.body.name;
            user.email = req.body.email;
            user.setPassword(req.body.password);
            user.save((err) => {
                if (err) {
                    res.render('register', {errorMessages: err});
                } else {
                    res.redirect('/login');
                }
            })
        }
    });

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
});

router.get('/auth/facebook',
    passport.authenticate('facebook', {authType: 'rerequest', scope: 'email'})
);

router.get('/auth/facebook/callback',
    passport.authenticate('facebook', {failureRedirect: '/login'}),
    (req, res) => {
        // Successful authentication, redirect home.
        res.redirect('/');
    });

module.exports = router;
