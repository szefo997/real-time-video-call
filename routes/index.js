const express = require('express');
const nodeMailer = require('nodemailer');
const config = require('../config');
const {check, validationResult} = require('express-validator');

const router = express.Router();
const transporter = nodeMailer.createTransport(config.mailer);

router.get('/', (req, res) => {
    res.render('index', {title: 'Home', home: true});
});

router.get('/about', (req, res) => {
    res.render('about', {title: 'About', about: true});
});

router.route('/contact')
    .get((req, res) => {
        res.render('contact', {title: 'Contact', contact: true});
    })
    .post([
        check('name').not().isEmpty().withMessage('Empty name'),
        check('email').isEmail().withMessage('Invalid email'),
        check('message').not().isEmpty().withMessage('Empty message')
    ], async (req, res) => {
        const errors = validationResult(req);
        if (errors.array().length) {
            const {name, email, message} = req.body;
            res.render('contact', {
                title: 'Contact',
                name,
                email,
                message,
                contact: true,
                errorMessages: errors.array(),
            });
        } else {
            const info = await transporter
                .sendMail({
                    from: 'noreply@sz.pl',
                    to: 'k.rozycki@onet.pl',
                    subject: 'You got a new message from visitor',
                    text: message
                })
                .catch(e => {
                    console.error(e);
                });

            console.log("Message sent: %s", info);

            res.render('thank', {title: 'Thank', contact: true});
        }
    });


module.exports = router;
