const socketIO = require('socket.io');
const ot = require('ot');
const roomList = {};

module.exports = (server) => {
    const io = socketIO(server);
    const str = 'This is a Markdown heading \n\nvar i = i + 1;';

    io.on('connection', (socket) => {
        socket.on('joinRoom', ({room, username}) => {
            console.warn(`socket join room: ${room}, username: ${username}, roomList[room]:`, roomList[room]);
            if (!roomList[room]) {
                roomList[room] = new ot.EditorSocketIOServer(str, [], room, function (socket, cb) {
                    Task.findOneAndUpdate({_id: room}, {
                        $set: {
                            content: this.document
                        }
                    }, (err) => {
                        if (err) return cb(false);
                        cb(true);
                    });
                });
            }
            roomList[room].addClient(socket);
            roomList[room].setName(socket, username);

            socket.room = room;
            socket.join(room);
        });

        socket.on('chatMessage', (data) => {
            console.warn('chat message', data);
            io.to(socket.room).emit('chatMessage', data);
        });

        socket.on('disconnect', () => {
            console.warn('disconnect');
            socket.leave(socket.room);
        });
    });
};
