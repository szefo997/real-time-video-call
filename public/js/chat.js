const EditorClient = ot.EditorClient;
const SocketIOAdapter = ot.SocketIOAdapter;
const CodeMirrorAdapter = ot.CodeMirrorAdapter;

const socket = io.connect('http://localhost:3000');
const editor = CodeMirror.fromTextArea(document.getElementById("code-screen"), {
    lineNumbers: true,
    theme: "darcula"
});

const $chatUsername = $("#chat-box-username");
const $userMessage = $('#user-message');

const roomId = $('#room-id').val();
const code = $('#code-screen').val();
const username = $chatUsername.text().trim() || $chatUsername.text("User" + Math.floor(Math.random() * 9999).toString()).text();
let cmClient;

socket.emit('joinRoom', {room: roomId, username});

const init = (str, revision, clients, serverAdapter) => {
    if (!code) {
        editor.setValue(str);
    }
    cmClient = window.cmClient = new EditorClient(revision, clients, serverAdapter, new CodeMirrorAdapter(editor));
};

const sendMessage = () => {
    const message = $userMessage.val();
    if (!message) {
        return;
    }
    socket.emit('chatMessage', {message, username});
    const list = document.getElementById("chat-box-listMessages");
    setTimeout(() => {
        list.scrollTop = list.scrollHeight;
    }, 5);
};

const userMessage = (name, text) => `<li class="list-group-item">${name}</b> : ${text}</li>`;

socket.on('doc', ({str, revision, clients}) => {
    console.warn(`socket on doc str: ${str}, revision: ${revision}, clients:`, clients);
    init(str, revision, clients, new SocketIOAdapter(socket));
});

socket.on('chatMessage', ({username, message}) => {
    console.warn(`socket on chatMessage username: ${username} message: ${message}`);
    $('#chat-box-listMessages').append(userMessage(username, message));
});
