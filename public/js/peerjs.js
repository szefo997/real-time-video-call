navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

// PeerJS object
const peer = new Peer(username + roomId);
// settings for own server
// {
//     host: 'rocky-depths-74216.herokuapp.com',
//         path: '/peerjs',
//     port: 443,
//     secure: true,
//     key: 'code4startup',
//     debug: true
// }

peer.on('open', () => {
    console.warn('peer open');
    $('#my-id').text(peer.id);
});

// Receiving a call
peer.on('call', call => {
    console.warn('peer call', call);
    // Answer the call automatically (instead of prompting user) for demo purposes
    call.answer(window.localStream);
    step3(call);
});

peer.on('error', err => {
    console.warn('peer err', err);
    alert(err.message);
    // Return to step 2 if error occurs
    step2();
});

// Click handlers setup
$(() => {
    $('#make-call').click(() => {
        // Initiate a call!
        const call = peer.call($('#callto-id').val(), window.localStream);
        step3(call);
    });
    $('#end-call').click(() => {
        window.existingCall.close();
        step2();
    });
    step1();
});

const step1 = () => {
    // Get audio/video stream
    navigator.getUserMedia({audio: true, video: true}, stream => {
        document.getElementById('my-video').srcObject = stream;
        window.localStream = stream;
        step2();
    }, () => {
        $('#step1-error').show();
    });
};

const step2 = () => {
    $('#step1, #step3').hide();
    $('#step2').show();
};

const step3 = (call) => {
    // Hang up on an existing call if present
    if (window.existingCall) {
        window.existingCall.close();
    }
    // Wait for the stream on the call, then set peer video display
    call.on('stream', stream => {
        document.getElementById('second-video').srcObject = stream;
    });
    // UI stuff
    window.existingCall = call;
    $('#second-id').text(call.peer);
    call.on('close', step2);
    $('#step1, #step2').hide();
    $('#step3').show();
};
